omero-napari
============

OMERO CLI client to open images in the napari viewer.

This project is deprecated.

It has been merged with https://github.com/tlambert03/napari-omero 
